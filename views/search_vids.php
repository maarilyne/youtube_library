<?php 
require "../controller/traitement.php";
require "../controller/library_functions.php";
 
//Calls function that removes a video from the playlist
if(isset($_GET['action'], $_GET['id'])){
    $user = $json -> name;
    $action = $_GET['action'];
    $id = $_GET['id'];
    $title = $_GET['title'];

    if($action == "del"){
        removeVideo($user, $id, $json);
    }

    if($action == "add"){
        addVideos($user, $id, $title, $json);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">  
        <title>Youtube library</title>
        <meta name="description" content="Library where you can save and watch all your favourite Youtube videos.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../styles/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="container">
        <a href="index.php?user=<?=strtolower($user)?>" class="list-access">Library's videos</a>
        <a href="login.php" class="logout">Logout</a>
            <!-- SIDE BAR MENU -->
            <header class="menu">
                <div class="head_subcontainers">
                    <span><?= $user; ?>'s library</span>
                </div>
                <div class="head_subcontainers reveal search-link-container">
                    <a href="search_vids.php?user=<?=strtolower($user)?>" class="search-link">[ + ]</a>
                </div>
                <!-- lists all the youtube videos saved by the user -->
                <?php 
                    for ($i=0; $i< count($json->videos); $i++):
                        $video_id = $json->videos[$i]->id;
                        $video_title = $json->videos[$i]->title;
                ?>
                    <div class="head_subcontainers videos_list">
                        <a href="index.php?user=<?=strtolower($user)?>&action=del&id=<?= $video_id?>" class="delete_btn">
                            [x]
                        </a>
                        <span class="video_titles"  data-ytId="<?= $video_id; ?>"  data-ytTitle="<?= $video_title; ?>">
                            <?= $json->videos[$i]->title; ?>
                        </span>
                    </div>
                <?php endfor; ?>
            </header>
            
            <!-- MAIN CONTENT -->
            <main id="main">
                <section class="main_subcontainers search_container">
                    <form action="search_vids.php?user=<?= $user?>" method="post">
                        <div class="searchBar_container">
                            <label class="form-labels">Search :</label>
                            <input type="text" class="search-bar" name="query">
                            <input type="submit" value="Enter" name="search-submit">                      
                        </div>
                        <div class="filterSearch_container">
                            <label class="form-labels"> 
                                <?php 
                                //If the user requested a number of results, the number will be shown
                                    if(!empty($nbResults)):?>
                                        Display <?= $queryParams['maxResults']; ?> results                             
                                <?php 
                                //If not, the number of result displayed will be "0"
                                    else:?>
                                        Display 0 results
                                <?php endif;?>
                            </label>
                            <input type="number" class='filter-nb' name="nbResults" min="1" required>
                        </div>
                    </form>
                </section>
                <section class="main_subcontainers results-container">               
                    <?php 
                    //If the user types something in the search bar, we'll display "n" youtube videos by topic with a foreach loop
                    if(!empty($query)):
                        foreach($response['items'] as $video): ?>
                            <div class="res-vids-container">
                                <a href="https://youtube.com/watch?v=<?= $video['id']['videoId']; ?>" class="res-link">
                                    <img src="<?= $video['snippet']['thumbnails']['high']['url'];?>" alt="<?= $video['snippet']['title']; ?>" class="res-img">
                                </a>
                                <div class="res-details">
                                    <h4><?= $video['snippet']['title']; ?></h4>
                                    <p class="res-description"><?= nl2br($video['snippet']['description']); ?></p>
                                </div>
                                <a href="search_vids.php?user=<?=strtolower($user);?>&action=add&title=<?= $video['snippet']['title'];?>&id=<?= $video['id']['videoId'];?>" class="add-btn">+</a>
                            </div>
                        <?php endforeach;
                    
                    //If no research was made, we'll show a quick message 
                    elseif(empty($query)):?>
                        <p style="text-align: center">No videos found.</p>
                    <?php endif; ?>
                </section>
            </main>

             <!--dialog box - delete video-->
            <div class="dialogBox_container">
                <p>Êtes vous sûr de vouloir supprimer cette vidéo ?</p>
                <button data-res="yes" class="confirm">Oui</button>
                <button data-res="no" class="confirm">Non</button>
            </div>
        </div>
    </body>
    <!-- Scripts -->
    <script src="../scripts/youtube_lib.js"></script>
</html>




