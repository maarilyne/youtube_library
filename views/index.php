<?php 
require "../controller/traitement.php";
require "../controller/library_functions.php";

//Calls function that removes a video from the playlist
if(isset($_GET['action'], $_GET['id'])){ 
    $action = $_GET['action'];
    $user = $json->name;
    $id = $_GET['id'];

    if($action == "del"){
        removeVideo( $user, $id, $json);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">  
        <title>Youtube library</title>
        <meta name="description" content="Library where you can save and watch all your favourite Youtube videos.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../styles/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="container">
            <a href="index.php?user=<?=strtolower($user)?>" class="list-access">Library's videos</a>
            <a href="login.php" class="logout">Logout</a>
            <!-- SIDE BAR MENU -->
            <header class="menu">
                <div class="head_subcontainers">
                    <span><?= $json->name; ?>'s library</span>
                </div>
                <div class="head_subcontainers reveal search-link-container">
                    <a href="search_vids.php?user=<?= $user?>" class="search-link">[ + ]</a>
                </div>

                <!-- lists all the youtube videos saved by the user -->
                <?php 
                    for ($i=0; $i< count($json->videos); $i++):
                        $video_id = $json->videos[$i]->id;
                        $video_title = $json->videos[$i]->title;
                ?>
                    <div class="head_subcontainers videos_list">
                        
                            <a href="index.php?user=<?=strtolower($user)?>&action=del&id=<?= $video_id?>" class="delete_btn">
                                [x]
                            </a>
                        <span class="video_titles"  data-ytId="<?= $video_id; ?>"  data-ytTitle="<?= $video_title; ?>">
                            <?= $json->videos[$i]->title; ?>
                        </span>
                    </div>
                <?php endfor; ?>
            </header>

            <!-- MAIN CONTENT -->
            <main id="main">
                <div class="main_subcontainers title_container">
                    <h1 class="title">Title</h1>                
                </div>
                <div class="main_subcontainers video_container">
                    <iframe id="video" src=""></iframe>
                </div>
            </main>

             <!--dialog box - delete video (OBSOLETE)-->
            <!--
            <div class="dialogBox_container">
                <p>Êtes vous sûr de vouloir supprimer cette vidéo ?</p>
                <button data-res="yes" class="confirm">Oui</button>
                <button data-res="no" class="confirm">Non</button>
            </div>-->
        </div>
    </body>
    <!-- Scripts -->
    <script src="../scripts/youtube_lib.js"></script>
</html>