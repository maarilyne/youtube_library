<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">  
        <title>Youtube library</title>
        <meta name="description" content="Library where you can save and watch all your favourite Youtube videos.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../styles/login.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="container">
            <h1>Youtube Library</h1>
            <form action="index.php" class="login-form" method="post">
                <?php if(isset($_GET['errors'])) :
                    $message_err = $_GET['errors'];
                ?>
                    <p class="mess_err"><?= $message_err; ?></p>
                <?php endif; ?>
                <label for="username">Username</label>
                <input type="text" name="username" required>
                <input type="submit" value="Login" name="login-submit" id="submit-btn">
            </form>
        </div>
    </body>
</html>