//Display selected youtube videos
let items = document.querySelectorAll(".video_titles")
let yt_videos = document.querySelector("#video")
let yt_titles = document.querySelector(".title")


for (let i = 0; i < items.length; i++){  
    items[i].addEventListener("click",function() {
        youtube_id = items[i].getAttribute('data-ytId')
        youtube_title = items[i].getAttribute('data-ytTitle')

        yt_videos.setAttribute("src", 'https://www.youtube.com/embed/'+ youtube_id +'')
        yt_titles.innerHTML = youtube_title
        console.log(youtube_title)
    });
}
