<?php
    //Calling the php file where the APIs' settings were created
    require "api_settings.php";

    //SEARCH FUNCTION - This function enables us to add Youtube video in our playlist
    if(isset($_POST['search-submit'])){
        $query = $_POST['query'];
        $nbResults = $_POST['nbResults'];
    
        //Search settings stored in an array
        $queryParams = [
            'q'=> $query, 
            'order' => 'relevance', 
            'maxResults' => htmlentities($nbResults), 
            'type' => 'video'
        ];
        $response = $youtube->search->listSearch('snippet', $queryParams);
    } 

    //REMOVE FUNCTION - This function enables us to delete a video from our playlist
    function removeVideo($user, $id, $json_file){
            $values = $json_file->videos;

            for($i=0; $i < count($values); $i++){
                if($values[$i]->id == $id){
                    unset($json_file->videos[$i]);
                    $json_file->videos = array_values($json_file->videos);
                }
            }
            file_put_contents('../models/'.$user.'.json', json_encode($json_file));
    }

    //ADD FUNCTION - This function enables us to add Youtube videos in our playlist
    function addVideos($user, $vid_id, $vid_title, $json_file){
        $new_row = array("title"=> $vid_title, "id" => $vid_id);
        array_push($json_file->videos, $new_row);

        //"array_values()" removes the indexes inserted in the array after encoding the php array in json
        $json_file->videos = array_values($json_file->videos);
        file_put_contents('../models/'.$user.'.json', json_encode($json_file));

        //After inserting the new videos in the $json_file array, we refresh the page
        header('location: ../views/search_vids.php?user='.strtolower($user).'');
        exit(0);
    }
?>